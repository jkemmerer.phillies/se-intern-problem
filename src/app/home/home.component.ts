import { Component, OnInit, ViewChild } from '@angular/core';
import { FormGroup, FormBuilder } from '@angular/forms';
import { ParkingRequestService } from '../services/parking-request/parking-request.service';
import { MatDialog } from '@angular/material/dialog';
import { SimpleOkModal } from '../simple-ok/simple-ok.component';

export interface IParkingRequestForm {
  firstName: string;
  lastName: string;
  org: string;
  lot: string;
}
@Component({
  selector: 'app-home',
  templateUrl: './home.component.html',
  styleUrls: ['./home.component.css'],
  providers: [ParkingRequestService]
})
export class HomeComponent implements OnInit {

  @ViewChild('lot') lot;
  parkingRequest: FormGroup;

  constructor(public dialog: MatDialog, public fb: FormBuilder, public prs: ParkingRequestService) {
    this.initializeParkingRequestForm();
  }

  ngOnInit() {
  }

  getNewParkingRequestForm(): FormGroup {
    const newParkingRequestForm: IParkingRequestForm = {
      firstName: '',
      lastName: '',
      org: '',
      lot: 'Lot A'
    };
    return this.fb.group(newParkingRequestForm);
  }

  initializeParkingRequestForm() {
    this.parkingRequest = this.getNewParkingRequestForm();
  }

  submitRequest() {

    const parkingRequestForm: IParkingRequestForm = this.parkingRequest.value;
    const newParkingRequest = {
      firstName: parkingRequestForm.firstName,
      lastName: parkingRequestForm.lastName,
      organization: parkingRequestForm.org,
      lot: parkingRequestForm.lot
    };

    const ok = this.dialog.open(SimpleOkModal, {
      width: '420px',
      data: `New parking request created for ${newParkingRequest.firstName} ${newParkingRequest.lastName} in ${newParkingRequest.lot}`
    });
    ok.afterClosed().subscribe(() => {
      this.initializeParkingRequestForm();
    });
  }
}
