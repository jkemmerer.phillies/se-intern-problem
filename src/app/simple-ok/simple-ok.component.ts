import { Component, Inject } from '@angular/core';
import { MatDialogRef, MAT_DIALOG_DATA } from '@angular/material';

@Component({
  // tslint:disable-next-line:component-selector
  selector: 'simple-ok-modal',
  templateUrl: './simple-ok.component.html',
  styleUrls: ['./simple-ok.component.css']
})
// tslint:disable-next-line:component-class-suffix
export class SimpleOkModal {
  message: string;

  constructor(public dialogRef: MatDialogRef<SimpleOkModal>, @Inject(MAT_DIALOG_DATA) public data: any) {
    this.message = data.message || data || 'error loading message';
  }

  ok(): void {
    this.dialogRef.close();
  }
}
